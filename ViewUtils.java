public class ViewUtils{
	public static LayoutInflater inflater;
        public static ProgressDialog createProgressDialog(Context context,String msg){
    	       inflater = LayoutInflater.from(context);
    	       View v = inflater.inflate(R.layout.selfdef_progress_dialog_layout, null);
    	       TextView txt = (TextView) v.findViewById(R.id.progress_dialog_tv);
    	       ProgressDialog dialog = new ProgressDialog(context);
    	       dialog.show();
    	       dialog.setContentView(v);
    	       if(msg!=null&&!msg.equals(""))
    		    txt.setText(msg);
    	       return dialog;
       }